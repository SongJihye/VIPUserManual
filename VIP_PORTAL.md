# VIP

### VIP : Vessel Integrated Portal.

### 1. Home

The first page when you login the VIP.

- **[HOME](HOME.md)** Click when you move to the Main page at each menu.
- **[ADMIN](ADMIN.md)** Click when you move to Admin. Menu.(Ref. 16 Admin Manual)
- **[SITEMAP](SITEMAP.md)** The perspective Menu structure for VIP PORTAL.
- **[CAS](CAS.md)** Crew Administration System Menu.
- **[MMS](MMS.md)** Maintenance Management System Menu.
- **[FMS](FMS.md)** Fleet Management System Menu.
- **[EDMS](EDMS.md)** Electronic Document Management System Menu.
- **[DMS](DMS.md)** Drawing Management System Menu.
- **[SHIPMAIL](SHIPMAIL.md)** Mail System for Ship & Ground.
- **[APPLOVAL](APPROVAL.md)** Check the Approval document s.
- **[BORAD](BOARD.md)** Bulletin Board for notice.

### 2. User Information

Display the simple basic information about the login user.

- **User Name** Display login user’s name.
- **Logout** Log out from the VIP Portal.
- **Hide** Tap for hiding left menu.(It’s useful when use the FMS.)
- **Visa** Display expiration date of Visa.
- **Passport** Display expiration date of Passport.
- **Seaman** Nation Display the nationality of the login user.
- **Seaman** Flag Display the name of login user.
- **Crew Mail Limit** Display the mail capacity of current user..
- **Quick Launch** Display the list of the favorite sites that the user was enrolled.

### 3. Slogan / Policy

The Manager of the ship can enroll the matters that required attention, and show the message from the head quarter to the ship.

- **Slogan** Display the policy of the matters that required attention to the all the crews.
- **Edit** Edit, and register the slogan.(Ref. 3.1)
- **HMM Company Policy** Show the policy from the head quarter. 

#### 3.1	Slogan, Policy Popup

Display the enrolled message and can edit that.
- **Save** Save the edited message. 
- **Close** Close the window of the edited page.